
/* Generated data (by glib-mkenums) */

#ifndef __LUNAR_ENUM_TYPES_H__
#define __LUNAR_ENUM_TYPES_H__

#include <glib-object.h>

G_BEGIN_DECLS

/* Enumerations from "lunar-date.h" */

#define LUNAR_TYPE_DATE_ERROR	(lunar_date_error_get_type())
GType lunar_date_error_get_type	(void) G_GNUC_CONST;

/* Enumerations from "lunar-calendar.h" */

#define LUNAR_TYPE_CALENDAR_DISPLAY_OPTIONS	(lunar_calendar_display_options_get_type())
GType lunar_calendar_display_options_get_type	(void) G_GNUC_CONST;

G_END_DECLS

#endif /* __LUNAR_ENUM_TYPES_H__ */

/* Generated data ends here */

