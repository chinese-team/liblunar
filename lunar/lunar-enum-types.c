
/* Generated data (by glib-mkenums) */

#include "lunar-enum-types.h"

/* enumerations from "lunar-date.h" */
#include "lunar-date.h"

GType
lunar_date_error_get_type (void)
{
	static GType the_type = 0;
	
	if (the_type == 0)
	{
		static const GEnumValue values[] = {
			{ LUNAR_DATE_ERROR_INTERNAL,
			  "LUNAR_DATE_ERROR_INTERNAL",
			  "internal" },
			{ LUNAR_DATE_ERROR_DAY,
			  "LUNAR_DATE_ERROR_DAY",
			  "day" },
			{ LUNAR_DATE_ERROR_YEAR,
			  "LUNAR_DATE_ERROR_YEAR",
			  "year" },
			{ LUNAR_DATE_ERROR_LEAP,
			  "LUNAR_DATE_ERROR_LEAP",
			  "leap" },
			{ 0, NULL, NULL }
		};
		the_type = g_enum_register_static (
				g_intern_static_string ("LunarDateError"),
				values);
	}
	return the_type;
}

/* enumerations from "lunar-calendar.h" */
#include "lunar-calendar.h"

GType
lunar_calendar_display_options_get_type (void)
{
	static GType the_type = 0;
	
	if (the_type == 0)
	{
		static const GFlagsValue values[] = {
			{ LUNAR_CALENDAR_SHOW_HEADING,
			  "LUNAR_CALENDAR_SHOW_HEADING",
			  "show-heading" },
			{ LUNAR_CALENDAR_SHOW_DAY_NAMES,
			  "LUNAR_CALENDAR_SHOW_DAY_NAMES",
			  "show-day-names" },
			{ LUNAR_CALENDAR_NO_MONTH_CHANGE,
			  "LUNAR_CALENDAR_NO_MONTH_CHANGE",
			  "no-month-change" },
			{ LUNAR_CALENDAR_SHOW_WEEK_NUMBERS,
			  "LUNAR_CALENDAR_SHOW_WEEK_NUMBERS",
			  "show-week-numbers" },
			{ LUNAR_CALENDAR_WEEK_START_MONDAY,
			  "LUNAR_CALENDAR_WEEK_START_MONDAY",
			  "week-start-monday" },
			{ LUNAR_CALENDAR_SHOW_LUNAR,
			  "LUNAR_CALENDAR_SHOW_LUNAR",
			  "show-lunar" },
			{ 0, NULL, NULL }
		};
		the_type = g_flags_register_static (
				g_intern_static_string ("LunarCalendarDisplayOptions"),
				values);
	}
	return the_type;
}


/* Generated data ends here */

